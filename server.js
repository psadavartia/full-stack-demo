//require the express nodejs module
var express = require('express'),
	//set an instance of exress
	app = express(),
	//require the body-parser nodejs module
	bodyParser = require('body-parser'),
	//require the path nodejs module
	path = require('path');
//require axios
var axios = require('axios');
var cache = require('./cache/cache.js');
const bluebird = require("bluebird");

//support parsing of application/json type post data
app.use(bodyParser.json());

//support parsing of application/x-www-form-urlencoded post data
app.use(bodyParser.urlencoded({ extended: true })); 

//tell express that www is the root of our public web folder
app.use(express.static(path.join(__dirname, 'www')));

const url =
	"https://experimentation.getsnaptravel.com/interview/hotels";

function intersection(a, b) {
	var d = {};
	var results = [];
	for (var i = 0; i < b.length; i++) {
		d[b[i]] = true;
	}
	for (var j = 0; j < a.length; j++) {
		if (d[a[j]])
			results.push(a[j]);
	}
	return results;
}

function cachedHotelProperty(req) {
	cache.get(req.toString(), function (err, data) {
		if (err) throw err;
		if (data != null) {
			return data;
		} else {
			return null;
		}
	});
}

function getAllHotelPricesFromCacheOrRetrieveFromSource(providerIndicator, req) {
	var request = {
		city: req.body.city || null,
		checkin: req.body.checkIn || null,
		checkout: req.body.checkOut || null,
		provider : providerIndicator
	};
	var tobeReturned = {};
	var cachedResponse = cachedHotelProperty(request);
	if (cachedResponse != null) {
		tobeReturned[providerIndicator] = cachedResponse;
		return tobeReturned;
	}

	return axios.post(url, request)
		.then(function (response) {
			cache.set(request.toString(), response.data.toString());
			tobeReturned[providerIndicator] = response.data;
			return tobeReturned;
	}).catch(function (error) {
		console.log(error);
	});
}

//compare/prices to invoke parallel calls to obtain the response from snaptravel and hotels.com and form the result.
app.post('/compare/prices',function(req, res){
	
	Promise
		.all([getAllHotelPricesFromCacheOrRetrieveFromSource('snaptravel', req), 
				getAllHotelPricesFromCacheOrRetrieveFromSource('retail', req)])
		.then(function (hotelResponse) {
			console.log("Done with calls !");
			console.log(hotelResponse);

			var snapList = hotelResponse[0].snaptravel.hotels;
			var hotelsList = hotelResponse[1].retail.hotels;

			// Obtain the Common Ids
			var idsObtainedFromSnapTravel = snapList.map(a => a.id);
			console.log(idsObtainedFromSnapTravel);
			var idsObtainedFromHotels = hotelsList.map(a => a.id);
			console.log(idsObtainedFromHotels);
			var commonIds = intersection(idsObtainedFromSnapTravel, idsObtainedFromHotels);
			console.log(commonIds);
			
			// Remove the listings to not be printed
			var toKeep = new Set(commonIds);
			var snapListToPrint = snapList.filter(obj => toKeep.has(obj.id));
			var hotelsListToPrint = hotelsList.filter(obj => toKeep.has(obj.id));

			//Form response for the common Ids
			for (var i=0; i < snapListToPrint.length; i++) {
				for (var j=0; j < hotelsListToPrint.length; j++) {
					if (snapListToPrint[i].id == hotelsListToPrint[j].id) {
						snapListToPrint[i].hotelPrice = hotelsListToPrint[j].price;
						break;
					}
				}
			}
			console.log(JSON.stringify(snapListToPrint));
			res.send(snapListToPrint);
		})
		.catch(function (error) {
		console.log(error);
		});
});

//wait for a connection on port 3000
app.listen(3000, function () {
  console.log('Server is running. Point your browser to: http://localhost:3000');
});
