(function () {

  function init(){
    $('#submitButton').click(submitButtonHandler);
  }

  function submitButtonHandler (evt) {
     var search = document.getElementById('search');

      //prevent form submission
      evt.preventDefault();
      evt.stopPropagation();

      $('#post-results-container').fadeOut();
      $('.ajaxLoader').css('display', 'inline-block');


      //make the AJAX call
      $.ajax({
          url: '/compare/prices',
          type: 'POST',
          data: {
              city : search.city.value,
              checkIn : search.checkIn.value,
              checkOut : search.checkOut.value
          },
          success: postSuccessHandler
      });
  }

  function postSuccessHandler (jsonData) {
    var $data = $('#post-results-container .data');

    //reset the UI
    $data.html('');
    $('.ajaxLoader').hide();

    //update the UI with the data returned from the AJAX call with Hotel Listings
    $.each(jsonData, function () {
        $data.append('<li><b>' +  this.id + ' ');
        $data.append('<b>' +  this.hotel_name + ' ');
        $data.append('<b>' +  this.num_reviews + ' ');
        $data.append('<b>' +  this.address + ' ');
        $data.append('<b>' +  this.num_stars + ' ');
        $data.append('<b>' +  this.amenities.toString() + ' ');
        $data.append('<b>' +  this.image_url + ' ');
        $data.append('<b><font color="green">' +  this.price + ' </font>');
        $data.append('<b><font color="#8b0000">' +  this.hotelPrice + '</font></li>');
    });

    $('#post-results-container').fadeIn();
  };

//init on document ready
$(document).ready(init);
})();
